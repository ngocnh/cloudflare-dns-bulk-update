<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $client = new Client();
            $base_url = 'https://api.cloudflare.com/client/v4';
            $key = env('CLOUDFLARE_API_KEY');
            $email = env('CLOUDFLARE_API_EMAIL');
            $zone_id = env('CLOUDFLARE_ZONE_ID');
            $old_ip = Storage::exists('my.ip') ? Storage::get('my.ip') : '127.0.0.1';
            $my_ip = $client->get(env('GETIP_SERVICE'))->getBody()->getContents();

            if ($my_ip !== $old_ip) {
                $headers = [
                    //'X-Auth-Key' => $key,
                    //'X-Auth-Email' => $email,
                    'Authorization' => "Bearer {$key}",
                    'Content-Type' => 'application/json'
                ];

                $res = $client->request('GET', "{$base_url}/zones/{$zone_id}/dns_records", [
                    'headers' => $headers,
                    'query' => [
                        'name' => 'contains:'.env('CLOUDFLARE_SUB_DOMAIN'),
                        'type' => 'A',
                        'per_page' => 50,
                        'match' => 'all'
                    ]
                ]);

                $results = json_decode($res->getBody()->getContents(), true);
                $old = Storage::exists('cloudflare.dns.old') ? Storage::get('cloudflare.dns.old') : false;
                $old = $old ? unserialize($old) : [];
                $saved = [];

                foreach ($results['result'] as $result) {
                    $check = explode(env('CLOUDFLARE_DOMAIN'), $result['name']);
                    $saved[$result['name']] = $result['content'];

                    if (
                        count($check) > 1 &&
                        (
                            (isset($old[$result['name']]) && $old[$result['name']] !== $my_ip) ||
                            !isset($old[$result['name']])
                        )
                    ) {
                        $client->put("$base_url/zones/$zone_id/dns_records/{$result['id']}", [
                            'headers' => $headers,
                            'json' => [
                                'content' => $my_ip,
                                'type' => $result['type'],
                                'name' => $result['name'],
                                'ttl' => $result['ttl']
                            ]
                        ]);
                    }
                }

                Storage::put('cloudflare.dns.old', serialize($saved));
            }

            Storage::put('my.ip', $my_ip);
        })->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}

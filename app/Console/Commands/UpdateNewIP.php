<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class UpdateNewIP extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cf:update-ip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A application made by OsinBot for automatic update CloudFlare DNS IP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client();
        $this->base_url = 'https://api.cloudflare.com/client/v4';
        $this->key = env('CLOUDFLARE_API_KEY');
        $this->email = env('CLOUDFLARE_API_EMAIL');
        $this->domain = env('CLOUDFLARE_DOMAIN');
        $this->zone_id = env('CLOUDFLARE_ZONE_ID');
        $this->ignore_domain = env('CLOUDFLARE_IGNORE_DOMAINS');
        $this->ignore_sub_domain = env('CLOUDFLARE_IGNORE_SUB_DOMAINS');

        $this->ignore_domains = [];
        $this->ignore_sub_domains = [];
        $this->old_ip = Storage::exists('my.ip') ? Storage::get('my.ip') : '127.0.0.1';
        $this->my_ip = $this->client->get(env('GETIP_SERVICE'))->getBody()->getContents();

        $this->headers = [
            'X-Auth-Key' => $this->key,
            'X-Auth-Email' => $this->email,
            'Content-Type' => 'application/json'
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("====== OsinBot - CloudFlare Update IP ======");
        $this->info($this->description);
        $this->info("");
        $this->info("Old IP: $this->old_ip");
        $this->info("New IP: $this->my_ip");
        $ignore_domains = [];
        $ignore_sub_domains = [];

        if (!empty($this->ignore_domain)) {
            $ignore_domains = explode(';', $this->ignore_domain);
            $ignore_domains = count($ignore_domains) > 1 ? $ignore_domains : explode(',', $this->ignore_domain);
        }

        if (!empty($this->ignore_sub_domain)) {
            $ignore_sub_domains = explode(';', $this->ignore_sub_domain);
            $ignore_sub_domains = count($ignore_sub_domains) > 1 ? $ignore_sub_domains : explode(',', $this->ignore_sub_domain);
        }

        $this->info("Ignore Domains: $this->ignore_domain");
        $this->info("Ignore Sub-Domains: $this->ignore_sub_domain");
        $this->info("=== Bot is Begining ===");
        $this->info("");

        if ($this->my_ip !== $this->old_ip) {
            $this->update_dns($ignore_domains, $ignore_sub_domains);
        } else {
            $this->info("No ip update available, bot stoped");
        }

        Storage::put('my.ip', $this->my_ip);
        $this->info("Stored new IP($this->my_ip to) " . Storage::path('my.ip'));
        $this->info("");
        $this->info("=== Bot is Ended ===");
    }

    private function request_dns($page)
    {
        $res = $this->client->request('GET', "{$this->base_url}/zones/{$this->zone_id}/dns_records", [
            'headers' => $this->headers,
            'query' => [
                'name' => "contains:{$this->domain}",
                'type' => 'A',
                'page' => $page,
                'per_page' => 50,
                'match' => 'all'
            ]
        ]);

        $results = json_decode($res->getBody()->getContents(), true);

        return $results;
    }

    private function update_dns($ignore_domains, $ignore_sub_domains, $page = 1)
    {
        $results = $this->request_dns($page);
        $total_page = $results['result_info']['total_pages'];
//        $old = Storage::exists('cloudflare.dns.old') ? Storage::get('cloudflare.dns.old') : false;
//        $old = $old ? unserialize($old) : [];
        $saved = [];

        foreach ($results['result'] as $result) {
            $saved[$result['name']] = $result['content'];

            if (
                $this->verify_domain($result['name'])
                && $this->check_ignore_domains($result['name'], $ignore_domains)
                && $this->check_ignore_sub_domains($result['name'], $ignore_sub_domains)
//                || !isset($old[$result['name']])
            ) {
                $res = $this->client->put("$this->base_url/zones/$this->zone_id/dns_records/{$result['id']}", [
                    'headers' => $this->headers,
                    'json' => [
                        'content' => $this->my_ip,
                        'type' => $result['type'],
                        'name' => $result['name'],
                        'ttl' => $result['ttl']
                    ]
                ]);

                if ($res->getStatusCode() == 200) {
                    $this->info("Updated {$result['name']} IP to $this->my_ip");
                } else {
                    $this->error("Request update for {$result['name']} has failed");
                }
            } else {
                $this->info("Ignore {$result['name']} in list");
            }
        }

        if ($page < $total_page) {
            $page++;
            $this->update_dns($ignore_domains, $ignore_sub_domains, $page);
        }

        Storage::put('cloudflare.dns.old', serialize($saved));
    }

    private function verify_domain($domain): bool
    {
        $check = explode($this->domain, $domain);
        return count($check) > 1;
    }

    private function check_ignore_domains($domain, $ignore_domains): bool
    {
        foreach ($ignore_domains as $ignore_domain) {
            if ($domain == $ignore_domain) {
                return false;
            }
        }

        return true;
    }

    private function check_ignore_sub_domains($domain, $ignore_sub_domains): bool
    {
        foreach ($ignore_sub_domains as $ignore_sub_domain) {
            if (count(explode($ignore_sub_domain, $domain)) > 1 || $ignore_sub_domain == $domain) {
                return false;
            }
        }

        return true;
    }
}
